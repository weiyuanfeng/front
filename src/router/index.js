import Vue from 'vue'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import Router from 'vue-router'
import axios from 'axios'
import VueAxios from 'vue-axios'
import Index from '@/components/Index'
import Login from '@/components/Login'

Vue.use(Router)
Vue.use(VueAxios,axios);
Vue.use(ElementUI);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index
    },
    {
      path: '/admin/login',
      name: 'Login',
      component: Login
    }
  ]
})
